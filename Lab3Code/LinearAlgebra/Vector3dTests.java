package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

//Vinuyan Sivakolunthu 2037402

public class Vector3dTests {
    @Test
    public void getterXTest(){
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(1, vec.getX());
    }

    @Test
    public void getterYTest(){
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(2, vec.getY());
    }

    @Test
    public void getterZTest(){
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(3, vec.getZ());
    }

    @Test
    public void getMagnitudeTest(){
        Vector3d vec = new Vector3d(2, 3, 6);
        assertEquals(7, vec.getMagnitude());
    }

    @Test
    public void dotProducTest(){
        Vector3d vec1 = new Vector3d(1, 2, 3);
        Vector3d vec2 = new Vector3d(4, 5, 6);
        assertEquals(32, vec1.dotProduct(vec2));
    }

    @Test
    public void addTest(){
        Vector3d vec1 = new Vector3d(1, 2, 3);
        Vector3d vec2 = new Vector3d(4, 5, 6);
        Vector3d vec3 = new Vector3d(5, 7, 9);
        Vector3d vecTemp = vec1.add(vec2);
        assertEquals(vec3.getX(), vecTemp.getX());
        assertEquals(vec3.getY(), vecTemp.getY());
        assertEquals(vec3.getZ(), vecTemp.getZ());
    }
}
