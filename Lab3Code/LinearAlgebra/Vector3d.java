package LinearAlgebra;

//Vinuyan Sivakolunthu 2037402
public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double getMagnitude(){
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }

    public double dotProduct(Vector3d vec){
        return (this.x*vec.getX()) + (this.y*vec.getY()) + (this.z*vec.getZ());
    }

    public Vector3d add(Vector3d vec){
        double newX = this.x + vec.getX();
        double newY = this.y + vec.getY();
        double newZ = this.z + vec.getZ();
        Vector3d vectorSum = new Vector3d(newX, newY, newZ);
        return vectorSum;
    }
}